def buildJar() {
    echo "building the application..."
    sh 'mvn clean package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t haythambammou/my-repo:$IMAGE_NAME ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push haythambammou/my-repo:$IMAGE_NAME"
    }
} 

def ansible(){
    echo "calling ansible playbook to configure ec2 instances"
    def remote = [:]
    remote.name = "ansible-server"
    remote.host = ANSIBLE_SERVER
    remote.allowAnyHosts = true 
    withCredentials([sshUserPrivateKey(credentialsId:'ansible-ssh-key', keyFileVariable: "keyfile",usernameVariable: 'user')]){
       remote.user = user
       remote.identityFile = keyfile
       withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'DOCKERPASS', usernameVariable: 'DOCKERUSER')]){
          sshCommand remote: remote, command: "ansible-playbook --extra-vars='docker_password=${DOCKERPASS} image=${IMAGE_NAME}' my-playbook.yaml"
    }
}
}
def commit(){
    //    sh 'git remote set-url origin git@gitlab.com:bammouhaytham0/java-app.git'
       sh 'git add .'
       sh 'git config --global user.email "jenkins@example.com" '
       sh 'git config --global user.name "jenkins"'
       sh "git commit -m 'ci : version bump '"
       sh 'git push origin HEAD:master'
}
// test

return this