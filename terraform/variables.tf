variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable subnet_cidr_block {
    default = "10.0.1.0/24"
}
variable env_prefix {
    default = "jenkins"
}
variable ec2-type {
    default = "t2.micro"
}
variable region {
    default = "eu-west-2"
}
